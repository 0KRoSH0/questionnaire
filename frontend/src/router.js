import { createRouter, createWebHistory } from "vue-router";
import Questionnaire from '@/pages/Questionnaire';
import Form from '@/pages/Form';

export default createRouter({
	history: createWebHistory(),
	routes: [
		{ path: '/', component: Questionnaire },
		{ path: '/form', component: Form },
	]
})